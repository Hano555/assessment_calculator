using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PST_1 {
// Student Name : Hanosiyan Ravinthirakumaran
// Student ID : N10142975
// Overall Descrription about the program : The purpose of this program is to aid students improve in managing their time and study, by creating two calculators: one being Essay Grade Calculator(Managing study time by advising students how many words they should write per day) and the other being Assignemnt Grade Calculator(provides students with their marks for the assignment, lettign students how well they are studying).
    
    class Program { // This is where our program is going to be produced.
        

        static void Main(string[] args) {
            bool Mainfunction;
            do {
                WelcomeMessageToUser();
                Mainfunction = MainMenu(choice: numcheck(message: "test", max: 3, min: 3));
            } while (!Mainfunction); // This do-while loop will run as long as 3 is not entered as a choice in Main menu method, because 3 will return true and Main Menu will store the true value. When While(!MainFunction) runs, the value stores in MainFunction is going become true to due to storing Main menu in it, therefore it will output a value = false, resulting in jumping out of the loop ending the program.



        }

        // This method greets the user, display options to choose from and asks the suer to enter a num from 1-3 from the main menu method. If the user types in any number above 3 or below 1, the restriction method, numcheck will run asking user to re-enter values between 1-3.
        static void WelcomeMessageToUser() {
        
            Console.Write("What is the student's name?:\n ");
            string username = Console.ReadLine();
            Console.WriteLine("####### Hello! {0} and Welcome to Assessment Calculator 2018 ######", username);
            Console.WriteLine("Please choose from the following options:");
            Console.WriteLine("1. Essay Word Calculator");
            Console.WriteLine("2. Assignment Grade Calculator");
            Console.WriteLine("3. Exit Program\n");
            Console.Write(">>>>>>>>>>>Enter Your Choice: ");


        }
        static bool MainMenu( double choice) { // Main Menu method prompts user to choose one from one of the options. This method is a non-void bool method, because it will be used for the do-while loop in the Main Method. 
            if (choice == 1) { // If 1 is pressed, the user will be directed to Essay Grade Calculator method.
                EssayGradeCalculator();
            } else if (choice == 2) { // If 2 is pressed, the user will be directed to Assingment Grade Calculator method.
                AssignmentGradeCalculator();
            } else if (choice == 3) { // If 3 is pressed, the program will close gracefully.
                Console.WriteLine("Goodbye User!!!");
                return true;
            }
            return false;
        }
        // Method for Essay Grade Calculator is Created here!!!!//
        static void EssayGradeCalculator() {
            Console.WriteLine();
            Console.WriteLine("-------Essay Grade Calculator-------");

            Console.Write("How many words are required? ");
            double wordsrequired = numcheck(); // numcheck is being used here to do the restriction check to limit the user's input to only numerical.

            Console.Write("How many words have you written so far? ");
            double wordswritten = numcheck(); // numcheck is being used here to do the restriction check to limit the user's input to only numerical.

            // This if statement only occurs if user input for words written is bigger than words required to write for the assignment. This if statement will display a warning message to the user to check the words written entered.
            if (wordswritten > wordsrequired) { 
                Console.WriteLine("PLS RE-CHECK! You cannot have more words written than words required");
                Console.Write("How many words have you written so far? ");
                wordswritten = numcheck(); // numcheck is being used here to do the restriction check to limit the user's input to only numerical.
            }
                Console.Write("How many days before the due date? ");
            double daysremaining = numcheck(); // numcheck is being used here to do the restriction check to limit the user's input to only numerical.

            //Actual Calculations for Essay Grade Calculator begins here//
            double EssayCalculation = (wordsrequired - wordswritten) / daysremaining;
            Console.WriteLine("On average, you should write {0:F0} words each day.", EssayCalculation);

            Console.WriteLine("Press Enter to return back to main menu\n"); // Once the results have been displayed, a follow up display message will appear asking user to press Enter to return back to main menu.
            Console.ReadKey(); // reads the input user key.
            
        }

        // Assingment Calulator is produced here.
        public static void AssignmentGradeCalculator() {
            Console.WriteLine();
            Console.WriteLine("-------Assingment Grade Calculator-------");


            Console.Write("What is the assignment weighting as a percentage? ");
            double assignmentweighting = numcheck(min: 0, max: 100); // numcheck is being used here to do the restriction check to limit the user's input to only numerical. Also, the user can only type numbers between 0-100 due to restrictions. 

            Console.Write("How many assignment marks are possible? ");
            double assignedmarks = numcheck(); // numcheck is being used here to do the restriction check to limit the user's input to only numerical.

            Console.Write("How many assignment marks did you earn? ");
            double earnedassignemntmarks = numcheck( min: 0, max: 100, wholenumber: false); // numcheck is being used here to do the restriction check to limit the user's input to only numerical.
            

            

            // This if statement only occurs if user input for earned marks is bigger than possible marks for the assignment. This if statement will display a warning message to the user to check the earned marks entered.
            if (earnedassignemntmarks > assignedmarks) {
                Console.WriteLine("PLS RE-CHECK! You cannot have more marks earned than marks assigned for the assignment");
                Console.Write("How many assignment marks did you earn? ");
                assignedmarks = numcheck();

            }
            //Actual Calculations for Assignment Calculator begins here//
            Console.WriteLine(); 
            Console.WriteLine("-----RESULTS-----"); 
            double possibleassingmentmarksaspercentage =  (earnedassignemntmarks/assignedmarks) * 100 ; // i have possible marks as percentage stored in the varibale here for grade scaling later.
            double possibleassingmentmarks = (earnedassignemntmarks / assignedmarks); // i have another possible marks variable here not as percentage to later change into percentage when displaying to user.
            Console.WriteLine("You have earned {0:f2}% of the assignment marks.", possibleassingmentmarks * 100);
            double weightedpercentage = (possibleassingmentmarks * assignmentweighting);
            Console.WriteLine("You have earned {0:f2}% towards your unit grade ({1:f2}% possible)", weightedpercentage, assignmentweighting);
            double unitgrade = 0;

            // grade scaler uses the varible with possible marks as percentage here to compare possible marks percentage with grading percentage.
            if (possibleassingmentmarksaspercentage == 0) {
                unitgrade = 0;
            } else if ((possibleassingmentmarksaspercentage > 0) && (possibleassingmentmarksaspercentage <= 20)) {
                unitgrade = 1;
            } else if ((possibleassingmentmarksaspercentage > 20) && (possibleassingmentmarksaspercentage <= 30)) {
                unitgrade = 2;
            } else if ((possibleassingmentmarksaspercentage > 30) && (possibleassingmentmarksaspercentage <= 40)) {
                unitgrade = 3;
            } else if ((possibleassingmentmarksaspercentage > 40) && (possibleassingmentmarksaspercentage <= 50)) {
                unitgrade = 4;
            } else if ((possibleassingmentmarksaspercentage > 50) && (possibleassingmentmarksaspercentage <= 65)) {
                unitgrade = 5;
            } else if ((possibleassingmentmarksaspercentage > 65) && (possibleassingmentmarksaspercentage <= 75)) {
                unitgrade = 6;
            } else if ((possibleassingmentmarksaspercentage > 75) && (possibleassingmentmarksaspercentage <= 100)) {
                unitgrade = 7;
            }

            // This if statement only occurs if possible marks percentage is lower than 50% (unitgrade of 4) warning the user to step it up if he/she wants to pass.
            if (possibleassingmentmarksaspercentage <= 40) {
                Console.WriteLine("If you continue at this rate, you are on track for a unit grade of {0}", unitgrade);
                Console.WriteLine("You will need to step it up if you want to pass!");
            // If the user got anything above or equal to 50% will get his/her unitgrade without a warning message
            } else {
                Console.WriteLine("If you continue at this rate, you are on track for a unit grade of {0}", unitgrade);
            }

            Console.WriteLine("Press Enter to return back to main menu\n");  // Once the results have been displayed, a follow up display message will appear asking user to press Enter to return back to main menu.
            Console.ReadKey();// reads the input user key.
        }


        // The below method called, "numcheck"works as number checking. So what it basically does is, it creates a restriction for the user to limit his inputs. To do this we need to return something and thus becomes a reurning method(non-void).
        public static double numcheck(string message = "",
                                      double min = 0,
                                      double max = double.MaxValue,
                                      bool wholenumber = true
                                      ) {
                
            bool valid = false;
            int num = 0;
            do {
                string test = Console.ReadLine();

                if (double.TryParse(test, out min) == false) { // So whenever the user types in an input as a string, it will display a pop message saying the written input should be numerical.

                    Console.WriteLine("Input {0} must be an numerical", test);
                } else if (num >= min) { // check whether entered number is lower than 0. If it is, it will prompt the user to enter a number more than or equal to 0.
                    Console.WriteLine("Input {0} must be no less than min", test);

                } else if (max < min) { // check whether entered num is bigger than max. If it is, it will prompt the user to enter a number less than max.
                    Console.WriteLine("Input {0} must be no more than max", test);

                } else if (wholenumber && !IsWholeNumber(min)) { // this if statement will only occur if typed in number is not a whole number. If the number is not a whole number, then the user will be prompted to re-enter a whole number.
                    Console.WriteLine("Input {0} must be a whole number", test); 
                    
                } else {
                    valid = true; // this will happen if it successfully passes through the first three if statments and will be confirmed as a valid input from the user.
                }

            } while (!valid); // while the variable valid is true, this post while loop will run.
            return min; // returns the value.
        }


        static bool IsWholeNumber(double number) { // This method simply checks whether the double typed in by user is a whole number or not.
           
            return (int)number == number;// here it checks whether if int casted numbe is equilavent to double number is typed in. 
        }



    } // end of the program
} // end of namespace






            
                
                    
            
    

